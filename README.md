# OneDrive Package for Flysystem and Laravel

Based on the [now abandoned package by nicolasbeauvais](https://github.com/nicolasbeauvais/flysystem-onedrive) for Flysystem and OneDrive, but with some pre-done Laravel bits added.

## Installation

You can install the package via composer:

`composer require arris/laravel-onedrive`

## Usage

You'll need to create an [Azure App](https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationsListBlade) to use this driver.

Publish the config file :

`php artisan vendor:publish --tag=laravel-onedrive`

Set your `appId` and `appToken` in the config file.  You may also set your `tenantId` if required.

Your app is expected to store and retrieve the `authToken` and `refreshToken` values at run time.

Listen for the `Arris\OneDrive\Events\OneDriveTokensUpdated` event and store the provided `authToken` and `refreshToken` properties.

In your app's service provider boot method, retrieve and set these config values:

```
app('config')->set('onedrive.authToken', $retrievedAuthToken);
app('config')->set('onedrive.refreshToken', $retrievedRefreshToken);
```

The package provides built in scaffold for the OAuth2 flow to obtain these tokens, however _routes are not automatically provided_.  To use the built in controllers, add your routes as follows:
```
Route::get('onedrive/signin', '\Arris\OneDrive\Http\Controllers\OneDriveController@signin')->name('onedrive.signin');
Route::get('onedrive/callback', '\Arris\OneDrive\Http\Controllers\OneDriveController@callback')->name('onedrive.callback');
Route::get('onedrive/return', '\Arris\OneDrive\Http\Controllers\OneDriveController@oneDriveReturn')->name('onedrive.return');
```

And add those route names into your `config/onedrive.php` file.

Once you have obtained tokens, you can simply configure filesystems with the driver `onedrive`.

## Credits

This package is largely based on work by [Nicolas Beauvais](https://github.com/nicolasbeauvais).

## License

The MIT License (MIT).  