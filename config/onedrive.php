<?php

return [
    'appId' => env('ONEDRIVE_APPID', ''),
    'appSecret' => env('ONEDRIVE_APPSECRET', ''),
    'authToken' => '', // It is expected your app stores this and sets it at runtime
    'refreshToken' => '', // It is expected your app stores this and sets it at runtime
    'callbackRoute' => '', // The callback route to direct to after a token is validated
    'errorCallbackRoute' => '', // The callback route to direct to after a validation attempt fails
    'tenantId' => env('ONEDRIVE_APPTENANT', ''),
    'baseUrl' => env('ONEDRIVE_BASEURL', 'https://login.microsoftonline.com/'),
    'urlAuthorize' => env('ONEDRIVE_AUTHURL', '/oauth2/v2.0/authorize'),
    'urlAccessToken' => env('ONEDRIVE_TOKENURL', '/oauth2/v2.0/token'),
    'scopes' => env('ONEDRIVE_SCOPES', 'Files.ReadWrite.All'),
];
