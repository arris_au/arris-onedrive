<?php

namespace Arris\OneDrive\Http\Controllers;

use Arris\OneDrive\Events\OneDriveTokensUpdated;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use League\OAuth2\Client\Provider\GenericProvider;

class OneDriveController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getOAuthClient()
    {
        $baseUrl = config('onedrive.baseUrl');
        if (config('onedrive.tenantId')) {
            $baseUrl .= config('onedrive.tenantId');
        } else {
            $baseUrl .= 'common';
        }

        return new GenericProvider([
            'clientId' => config('onedrive.appId'),
            'clientSecret' => config('onedrive.appSecret'),
            'urlAuthorize' => $baseUrl.config('onedrive.urlAuthorize'),
            'urlAccessToken' => $baseUrl.config('onedrive.urlAccessToken'),
            'urlResourceOwnerDetails' => '',
            'scopes' => config('onedrive.scopes'),
        ]);
    }

    public function signin()
    {
        $oauthClient = $this->getOAuthClient();

        $authUrl = $oauthClient->getAuthorizationUrl();
        session(['oauthState' => $oauthClient->getState()]);

        return redirect()->away($authUrl);
    }

    public function callback(Request $request)
    {
        // Validate state
        $expectedState = session('oauthState');
        $request->session()->forget('oauthState');
        $providedState = $request->query('state');

        if (! isset($expectedState)) {
            // If there is no expected state in the session,
            // do nothing and redirect to the home page.
            return route(config('onedrive.errorCallbackRoute'));
        }

        if (! isset($providedState) || $expectedState != $providedState) {
            return redirect(route(config('onedrive.errorCallbackRoute')))
                ->with('error', 'Invalid auth state')
                ->with('errorDetail', 'The provided auth state did not match the expected value');
        }

        // Authorization code should be in the "code" query param
        $authCode = $request->query('code');
        if (isset($authCode)) {
            // Initialize the OAuth client
            $oauthClient = $this->getOAuthClient();

            try {
                // Make the token request
                $accessToken = $oauthClient->getAccessToken('authorization_code', [
                    'code' => $authCode,
                ]);

                OneDriveTokensUpdated::dispatch($accessToken->getToken(), $accessToken->getRefreshToken());

                return redirect(route(config('onedrive.callbackRoute')));
            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                return redirect(route(config('onedrive.errorCallbackRoute')))
                    ->with('error', 'Error requesting access token')
                    ->with('errorDetail', json_encode($e->getResponseBody()));
            }
        }

        return redirect(route(config('onedrive.errorCallbackRoute')))
            ->with('error', $request->query('error'))
            ->with('errorDetail', $request->query('error_description'));
    }
}
