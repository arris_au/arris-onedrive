<?php

namespace Arris\OneDrive\Providers;

use Arris\OneDrive\Adapters\OneDriveAdapter;
use Arris\OneDrive\Events\OneDriveTokensUpdated;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\OAuth2\Client\Provider\GenericProvider;
use Microsoft\Graph\Graph;

class OneDriveServiceProvider extends EventServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->loadRoutesFrom(__DIR__.'../../../routes/web.php');

        Storage::extend('onedrive', function ($app, $config) {
            $graph = new Graph();
            $graph->setAccessToken(config('onedrive.authToken'));
            try {
                $result = $graph->createRequest('GET', '/me/drive')->execute();
            } catch (\Exception $e) {
                $baseUrl = config('onedrive.baseUrl');
                if (config('onedrive.tenantId')) {
                    $baseUrl .= config('onedrive.tenantId');
                } else {
                    $baseUrl .= 'common';
                }

                $provider = new GenericProvider([
                    'clientId' => config('onedrive.appId'),
                    'clientSecret' => config('onedrive.appSecret'),
                    'urlAuthorize' => $baseUrl.config('onedrive.urlAuthorize'),
                    'urlAccessToken' => $baseUrl.config('onedrive.urlAccessToken'),
                    'urlResourceOwnerDetails' => '',
                    'scopes' => config('onedrive.scopes'),
                ]);
                $newToken = $provider->getAccessToken('refresh_token', [
                    'refresh_token' => config('onedrive.refreshToken'),
                ]);
                OneDriveTokensUpdated::dispatch($newToken->getToken(), $newToken->getRefreshToken());
                $graph->setAccessToken($newToken->getToken());
            }

            $adapter = new OneDriveAdapter($graph, 'root');

            return new Filesystem($adapter);
        });
    }

    public function register()
    {
        $this->publishes([
            __DIR__.'/../../config/onedrive.php' => config_path('onedrive.php'),
        ], 'laravel-onedrive');

        // Users can specify only the options they actually want to override
        $this->mergeConfigFrom(
            __DIR__.'/../../config/onedrive.php', 'onedrive'
        );
    }
}
