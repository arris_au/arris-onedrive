<?php

namespace Arris\OneDrive\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OneDriveTokensUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $accessToken;
    public $refreshToken;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($accessToken, $refreshToken)
    {
        $this->accessToken = $accessToken;
        $this->refreshToken = $refreshToken;
    }
}
